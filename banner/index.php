<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Acme&family=Carter+One&display=swap" rel="stylesheet">
    <title>Banner Cookies</title>
</head>
<body>
    <?php
        if(!$_POST['cookie']){
            ?>
            <form method="POST" action="/banner/index.php">
                <div class="banner">
                    <p>Ми використовуємо файли cookie</p>
                    <button type="submit" name="cookie" value="close">Закрити</button>
                </div>
            </form>
            <?php
        }else{
            echo "ok";
            setcookie("Cookies", "Ok", time() + 3600, "/");
        }
    ?>
    
</body>
</html>